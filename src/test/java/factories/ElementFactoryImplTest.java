package factories;

import models.Element;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ElementFactoryImplTest {

    private ElementFactory elementFactory;

    @Before
    public void init() {
        elementFactory = new ElementFactoryImpl();
    }
    @Test
    public void shouldCreateElementCorrectly() {
        String line = "Lampka%10%35%0.7%0.1%0.1%0.1%0.5";
        String separator = "%";
        Element element = elementFactory.createElement(line, separator);

        assertThat(element.getElementName()).isEqualTo("Lampka");
        assertThat(element.getPosition().getPositionX()).isEqualTo(10f);
        assertThat(element.getPosition().getPositionY()).isEqualTo(35f);
        assertThat(element.getPosition().getPositionZ()).isEqualTo(0.7f);
        assertThat(element.getSize().getLength()).isEqualTo(0.1f);
        assertThat(element.getSize().getHeight()).isEqualTo(0.1f);
        assertThat(element.getSize().getWidth()).isEqualTo(0.1f);
        assertThat(element.getWeight()).isEqualTo(0.5f);
    }
}