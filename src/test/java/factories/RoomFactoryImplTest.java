package factories;

import models.Room;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class RoomFactoryImplTest {

    private RoomFactory roomFactory;

    @Before
    public void setUp() {
        roomFactory = new RoomFactoryImpl();
    }

    @Test
    public void shouldCreateRoomCorrectly() {

        List<String> roomElements = getRoomDescription();
        String separator = "%";
        Room room = roomFactory.createRoom(roomElements, separator);

        assertThat(room.getName()).isEqualTo("Pokoj mamy");
        assertThat(room.getColorHex()).isEqualTo("#00ff00");
        assertThat(room.getColorHex()).isEqualTo("#00ff00");
        assertThat(room.getHeight()).isEqualTo(10.5f);
        assertThat(room.getArea()).isEqualTo(25);
        assertThat(room.getElements().size()).isEqualTo(2);
        assertThat(room.getElements().get(0).getElementName()).isEqualTo("Biurko");
        assertThat(room.getElements().get(0).getWeight()).isEqualTo(30.3f);
        assertThat(room.getElements().get(1).getElementName()).isEqualTo("Lampka");
        assertThat(room.getElements().get(1).getSize().getHeight()).isEqualTo(0.1f);
    }

    private List<String> getRoomDescription() {
        return Arrays.asList(
                "Pokoj mamy%#00ff00%10.5%25%2",
                "Biurko%10%3.5%0.0%2%1.5%0.7%30.3",
                "Lampka%10%35%0.7%0.1%0.1%0.1%0.5");
    }
}