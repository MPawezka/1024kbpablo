package factories;

import models.Element;
import models.Position;
import models.Size;
import models.properties.ElementProperty;

import java.util.EnumMap;
import java.util.Map;

import static models.properties.ElementProperty.*;

public class ElementFactoryImpl implements ElementFactory {
    @Override
    public Element createElement(String line, String separator) {
        if (line == null || line.isEmpty()) {
            throw new IllegalArgumentException("Element details should not be empty");
        }
        Map<ElementProperty, String> elementMap = createElementMap(line, separator);

        return new Element(
                elementMap.get(ELEMENT_NAME),
                getPosition(elementMap),
                getWeight(elementMap),
                getSize(elementMap)
        );
    }

    private float getWeight(Map<ElementProperty, String> elementMap) {
        return Float.parseFloat(elementMap.get(WEIGHT));
    }

    private Size getSize(Map<ElementProperty, String> elementMap) {
        return new Size(
                Float.parseFloat(elementMap.get(LENGTH)),
                Float.parseFloat(elementMap.get(WIDTH)),
                Float.parseFloat(elementMap.get(HEIGHT)));
    }

    private Position getPosition(Map<ElementProperty, String> elementMap) {
        return new Position(
                Float.parseFloat(elementMap.get(POSITION_X)),
                Float.parseFloat(elementMap.get(POSITION_Y)),
                Float.parseFloat(elementMap.get(POSITION_Z)));
    }

    private Map<ElementProperty, String> createElementMap(String line, String separator) {
        String[] lineProps = line.split(separator);
        Map<ElementProperty, String> elementMap = new EnumMap<>(ElementProperty.class);
        elementMap.put(ELEMENT_NAME, lineProps[0]);
        elementMap.put(POSITION_X, lineProps[1]);
        elementMap.put(POSITION_Y, lineProps[2]);
        elementMap.put(POSITION_Z, lineProps[3]);
        elementMap.put(LENGTH, lineProps[4]);
        elementMap.put(WIDTH, lineProps[5]);
        elementMap.put(HEIGHT, lineProps[6]);
        elementMap.put(WEIGHT, lineProps[7]);
        return elementMap;
    }
}
