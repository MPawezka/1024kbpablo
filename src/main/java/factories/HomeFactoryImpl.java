package factories;

import models.Home;
import models.Room;
import models.properties.HomeProperty;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import static models.properties.HomeProperty.*;

public class HomeFactoryImpl implements HomeFactory {
    private RoomFactory roomFactory = new RoomFactoryImpl();

    @Override
    public Home createHome(List<String> lines, String separator) {
        if (lines == null || lines.isEmpty()) {
            throw new IllegalArgumentException("Home detail list should not be empty");
        }
        String[] homeDetails = lines.get(0).split(separator);
        Map<HomeProperty, String> homeMap = createHomeMap(homeDetails);

        int numOfRooms = Integer.parseInt(homeMap.get(ROOMS_COUNT));
        List<Room> roomList = getRooms(lines, separator, numOfRooms);

        return new Home(homeMap.get(HOME_NAME), homeMap.get(ADDRESS), Integer.parseInt(homeMap.get(HOMEMADE_COUNT)),
                roomList);
    }

    private List<Room> getRooms(List<String> lines, String separator, int numOfRooms) {
        List<Room> roomList = new ArrayList<>();
        lines = lines.subList(1, lines.size()); // remove 1st row - home details

        Room room;
        for (int i = 0; i < numOfRooms; i++) {
            String[] roomDetails = lines.get(0).split(separator);
            int numOfRoomElements = getNumberOfElements(roomDetails);
            List<String> roomAndElements = lines.subList(0, numOfRoomElements + 1);
            room = roomFactory.createRoom(roomAndElements, separator);
            roomList.add(room);
            lines = lines.subList(1 + numOfRoomElements, lines.size());
        }
        return roomList;
    }

    private int getNumberOfElements(String[] roomDetails) {
        return Integer.parseInt(roomDetails[roomDetails.length - 1]);
    }

    private Map<HomeProperty, String> createHomeMap(String[] homeDetails) {
        Map<HomeProperty, String> homeMap = new EnumMap<>(HomeProperty.class);
        homeMap.put(HOME_NAME, homeDetails[0]);
        homeMap.put(ADDRESS, homeDetails[1]);
        homeMap.put(HOMEMADE_COUNT, homeDetails[2]);
        homeMap.put(ROOMS_COUNT, homeDetails[3]);
        return homeMap;
    }
}
