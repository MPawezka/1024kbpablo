package factories;

import models.Element;
import models.Room;
import models.properties.RoomProperty;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import static models.properties.RoomProperty.*;

public class RoomFactoryImpl implements RoomFactory {
    private ElementFactory elementFactory = new ElementFactoryImpl();

    @Override
    public Room createRoom(List<String> lines, String separator) {
        if (lines == null || lines.isEmpty()) {
            throw new IllegalArgumentException("Room detail list should not be empty");
        }
        Map<RoomProperty, String> roomMap = createRoomProperties(lines, separator);
        int numOfRoomElements = Integer.parseInt(roomMap.get(COUNT_ELEMENTS));
        List<Element> elementList = getRoomElements(lines, separator, numOfRoomElements);

        return new Room(
                roomMap.get(ROOM_NAME),
                roomMap.get(ROOM_COLOR_HEX),
                Float.parseFloat(roomMap.get(AREA)),
                Float.parseFloat(roomMap.get(HEIGHT)),
                elementList);
    }

    private List<Element> getRoomElements(List<String> lines, String separator, int numOfRoomElements) {
        List<Element> elementList = new ArrayList<>();
        Element element;
        for (int i = 0; i < numOfRoomElements; i++) {
            element = elementFactory.createElement(lines.get(i + 1), separator);
            elementList.add(element);
        }
        return elementList;
    }

    private Map<RoomProperty, String> createRoomProperties(List<String> lines, String separator) {
        String[] roomDetails = lines.get(0).split(separator);
        Map<RoomProperty, String> roomMap = new EnumMap<>(RoomProperty.class);
        roomMap.put(ROOM_NAME, roomDetails[0]);
        roomMap.put(ROOM_COLOR_HEX, roomDetails[1]);
        roomMap.put(HEIGHT, roomDetails[2]);
        roomMap.put(AREA, roomDetails[3]);
        roomMap.put(COUNT_ELEMENTS, roomDetails[4]);
        return roomMap;
    }
}
