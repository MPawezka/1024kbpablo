import factories.HomeFactory;
import factories.HomeFactoryImpl;
import models.Home;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Logger;

public class HomeLoaderImpl implements HomeLoader {
    private static Logger log = Logger.getAnonymousLogger();
    private HomeFactory homeFactory = new HomeFactoryImpl();

    private static HomeLoader homeLoader;

    static HomeLoader getInstance() {
        if (homeLoader == null) {
            homeLoader = new HomeLoaderImpl();
        }
        return homeLoader;
    }


    @Override
    public Home loadHome(String fileName, String separator) {
        //load file and use home factory to create home

        Home home = null;
        try {
            List<String> lines = Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);
            home = homeFactory.createHome(lines, separator);
            lines.forEach(System.out::println);
        } catch (IOException e) {
            log.info("File not found: " + e.getMessage());
        }

        return home;
    }
}
